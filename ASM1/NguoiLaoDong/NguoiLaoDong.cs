﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASM1.NguoiLaoDong;

public class NguoiLaoDong
{

    public string HoTen { get; set; }
    public int NamSinh { get; set; }
    public double LuongCoBan { get; set; }
    public NguoiLaoDong()
    {

    }

    public NguoiLaoDong(string hoTen, int namSinh, double luongCoBan)
    {
        HoTen = hoTen;
        NamSinh = namSinh;
        LuongCoBan = luongCoBan;
    }

    public void NhapThongTin(string hoTen, int namSinh, double luongCoBan)
    {
        HoTen = hoTen;
        NamSinh = namSinh;
        LuongCoBan = luongCoBan;

    }

    public double TinhLuong()
    {
        return LuongCoBan;
    }

    public void XuatThongTin()
    {
        Console.WriteLine($"Ho ten la: {HoTen}, nam sinh: {NamSinh}, luong co ban: {LuongCoBan}.");
    }
}
