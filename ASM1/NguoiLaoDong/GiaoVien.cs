﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASM1.NguoiLaoDong
{
    public class GiaoVien : NguoiLaoDong
    {
        public double HeSoLuong { get; set; }

        public GiaoVien() { }

        public GiaoVien(string hoTen, int namSinh, double luongCoBan, double heSoLuong)
            : base(hoTen, namSinh, luongCoBan)
        {
            HeSoLuong = heSoLuong;
        }

        public void NhapThongTin(string hoTen, int namSinh, double luongCoBan, double heSoLuong)
        {
            base.NhapThongTin(hoTen, namSinh, luongCoBan);
            HeSoLuong = heSoLuong;
        }

        public new double TinhLuong()
        {
            return LuongCoBan * HeSoLuong * 1.25;
        }

        public new void XuatThongTin()
        {
            base.XuatThongTin();
            Console.WriteLine($"He so luong: {HeSoLuong}, Luong: {TinhLuong()}");
        }

        public double XuLy()
        {
            return HeSoLuong + 0.6;
        }
    }
}
