﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASM1.NguoiLaoDong;

namespace ASM1.Program;

public class Program
{   static GiaoVien TimGVLuongThap(List<GiaoVien> giaoViens)
    {
        if (giaoViens.Count == 0)
        {
            return null;
        }

        GiaoVien gvLuongThap = giaoViens[0];

        foreach (GiaoVien giaoVien in giaoViens)
        {
            if (giaoVien.TinhLuong() < gvLuongThap.TinhLuong())
            {
                gvLuongThap = giaoVien;
            }
        }
        return gvLuongThap;
    }
    static void Main(string[] args)
    {
        int soLuong;
        List<GiaoVien> giaoViens = new List<GiaoVien>();
        do
        {
            Console.Write("Nhập số lượng giáo viên: ");
            soLuong = int.Parse(Console.ReadLine());
            if(soLuong < 1)
            {
                Console.WriteLine("Số lượng giáo viên phải lớn hơn 1 hoặc bằng 1. Vui lòng nhập lại.");

            }
        }while(soLuong <1);
        

        
        for (int i = 0; i < soLuong; i++)
        {
            Console.WriteLine($"Nhập thông tin giáo viên {i + 1}:");
            Console.Write("Họ tên: ");
            string hoTen = Console.ReadLine();
            int namSinh = 0;
            double luongCoBan = 0.0;
            double heSoLuong = 0.0;
            try
            {
                Console.Write("Năm sinh: ");
                namSinh = int.Parse(Console.ReadLine());
                if(namSinh <0)
                {
                    Console.WriteLine("Năm sinh không thể âm");
                }

                Console.Write("Lương cơ bản: ");
                luongCoBan = double.Parse(Console.ReadLine());
                if (namSinh < 0)
                {
                    Console.WriteLine("Lương cơ bản không thể âm");
                }

                Console.Write("Hệ số lương: ");
                heSoLuong = double.Parse(Console.ReadLine());
                if (namSinh < 0)
                {
                    Console.WriteLine("Hệ số lương không thể âm");
                }
            }
            catch(Exception e)
            {
                Console.WriteLine("Lỗi: " + e.Message);
                Console.WriteLine("Xin hãy nhập lại");
                i--;
                continue;
            }
            GiaoVien giaoVien = new GiaoVien(hoTen, namSinh, luongCoBan, heSoLuong);
            giaoViens.Add(giaoVien);
        }
        GiaoVien gvLuongThap=TimGVLuongThap(giaoViens);
        if (gvLuongThap != null)
        {
            Console.WriteLine("Thông tin giáo viên có lương thấp nhất:");
            gvLuongThap.XuatThongTin();
            Console.WriteLine($"Kết quả xử lý: {gvLuongThap.XuLy()}");
        }
        else { Console.WriteLine("Không có giáo viên nào trong danh sách."); }
        
        Console.Write("Ấn bàn phím để kết thúc chương trình...");
        Console.ReadLine();
    }
}
